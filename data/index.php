<?php

require "source/app.php";

$skin = isset($_COOKIE["skin"]);    //this is used to echo class for html according to the kin which is used at the moment by the user

?>
<!DOCTYPE html>
<html class="<?php if ($skin) echo 'day';?>" lang="cs">
    <head>
        <title>
            Vítejte na FELchatu!
        </title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="source/felchat-style.css">
    </head>
    <body>
        <header>
            <a class="topbar" href="index.php"><h1>FELchat</h1></a>
            <ul class="<?php if ($user->userLogged !== null) echo 'toolbar'; else echo 'hidden';?>" id="links-left">    
                <li><a href="profil.php"><h1>
                    <?php
                    if ($user->userLogged === null) {
                        echo 'Not Empty Heading';
                    } else {
                        $name = $user->userLogged["username"]; 
                        if (strlen($name) <= 12) {
                            echo ($name);
                        } else {
                            $shrtName = substr($name, 0, 9);
                            echo ($shrtName); ?>...<?php }
                    }
                    ?></h1></a></li>
                <li class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>"><a href="uzivatele.php?u=0&a=0"><h1>Uživatelé</h1></a></li>
                <li><a href="logOut.php" id="pseudo-links-right"><h1>Odhlášení</h1></a></li> 
            </ul> 
            <ul class="<?php if ($user->userLogged !== null) echo 'hidden'; else echo 'toolbar';?>" id="links-right">   
                <li><a href="registrace.php"><h1>Registrace</h1></a></li>
                <li><a href="prihlaseni.php"><h1>Přihlášení</h1></a></li>
            </ul>
            <ul id="roombar" class="show">
                <li><h1>Místnosti:</h1></li>
                <li><a href="global.php"><h1>/ Globální chat /</h1></a></li>
                <li><a href="skola.php"><h1>/ Škola /</h1></a></li>
                <li><a href="gaming.php"><h1>/ Gaming /</h1></a></li>
                <li><a href="hobby.php"><h1>/ Hobby /</h1></a></li>
                <li><a href="pap.php"><h1>/ Párty a posezení /</h1></a></li>
            </ul>
        </header>
        <main>            
            <div class="big">
                <h2>Vítejte na FELchatu!</h2>
                <h3>Chat pro všechny správné FELáky.</h3> 
            </div>
            <div class="maininfo">
                <h2>Co je to FELchat?</h2>
                <h3>Virtuální místo, kde se můžou scházet všichni FELáci, bez ohledu na věk, titul, katedru nebo obor. Můžou spolu sdílet své zkušenosti, zábavné i smutné historky a pomáhat si navzájem.</h3>
                <h2>Proč používat FELchat, když mám facebook?</h2>
                <h3>
                    Především anonymita ale taky to, že je to lightweight klient využívající servery ČVUT. Nemusíte se bát, že Mark si bude koukat,
                    které profesory nemáte rádi, nebo kteří/é spolužáci/čky se vám líbí. Diskuze se všemi o všem. Technické i sociální. Všechno na jednom místě.
                </h3>
                <h2>Kto je FELák?</h2>
                <h3>
                    Odpověď na tuto otázku není jednoduchá, avšak dá se s jistotou tvrdit, že každý FELák je přátelský student nebo přátel studentů ČVUT. Jestli jsi si pořád není jistý, jestli jsi správnej FELák,
                    můžeš si kouknout video, které ti možná osvětlí, co to to FELáctví vlastně je. >>>
                </h3>
            </div>
            <div class="side">
                <iframe width="712" height="400" src="https://www.youtube.com/embed/3mABLG6sUN0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
            </div>
        </main>
        <footer>
            
        </footer>

        <script src="source/FELchat.js"></script>
    
    </body>
</html>