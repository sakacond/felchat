<?php

require "source/app.php";

function escape($value) {
    echo htmlspecialchars($value, ENT_QUOTES);
}

$skin = isset($_COOKIE["skin"]);

$formSent = isset($_POST["textMessage"]);
$userCanPost = false;

if ($formSent) {
    $text = $_POST["textMessage"];
    $userCanPost = $db->userExists($user->userLogged["username"]) && !$db->userIsBanned($user->userLogged["username"]);
    if(strlen($text) > 0 && $userCanPost) {
        $messageP->newMessage(date("d/m/Y (h:i:sa)"), $user->userLogged["username"], $text);
        header("Location: pap.php");
        exit();
    }
}

$delFormSent = isset($_POST["tagDelete"]);
$messageExists = true;

if($delFormSent) {
    $tag = $_POST["tagDelete"];
    $messageExists = $messageP->messageExists($_POST["tagDelete"]);
    if($db->adminExists($user->userLogged["username"]) && $messageExists) {
        $messageP->deleteMessage($tag);
        header("Location: pap.php");
        exit();
    }
}

?>

<!DOCTYPE html>
<html class="<?php if ($skin) echo 'day';?>" lang="cs">
    <head>
        <title>
            Párty a posezení
        </title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="<?php if($db->adminExists($user->userLogged["username"])) echo 'source/felchat-style-admin.css'; else echo'source/felchat-style.css'; ?>">
    </head>
    <body>
        <header>
            <a class="topbar" href="index.php"><h1>FELchat</h1></a>
            <ul class="<?php if ($user->userLogged !== null) echo 'toolbar'; else echo 'hidden';?>" id="links-left">    
                <li><a href="profil.php"><h1>
                    <?php
                    if ($user->userLogged === null) {
                        echo 'Not Empty Heading';
                    } else {
                        $name = $user->userLogged["username"]; 
                        if (strlen($name) <= 12) {
                            echo ($name);
                        } else {
                            $shrtName = substr($name, 0, 9);
                            echo ($shrtName); ?>...<?php }
                    }
                    ?></h1></a></li>
                <li class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>"><a href="uzivatele.php?u=0&a=0"><h1>Uživatelé</h1></a></li>
                <li><a href="logOut.php" id="pseudo-links-right"><h1>Odhlášení</h1></a></li> 
            </ul> 
            <ul class="<?php if ($user->userLogged !== null) echo 'hidden'; else echo 'toolbar';?>" id="links-right">   
                <li><a href="registrace.php"><h1>Registrace</h1></a></li>
                <li><a href="prihlaseni.php"><h1>Přihlášení</h1></a></li>
            </ul>
            <ul id="roombar" class="show">
                <li><h1>Místnosti:</h1></li>
                <li><a href="global.php"><h1>/ Globální chat /</h1></a></li>
                <li><a href="skola.php"><h1>/ Škola /</h1></a></li>
                <li><a href="gaming.php"><h1>/ Gaming /</h1></a></li>
                <li><a href="hobby.php"><h1>/ Hobby /</h1></a></li>
                <li><a href="pap.php"><h1>/ Párty a posezení /</h1></a></li>
            </ul>
        </header>
        <main>
            <div class="bigname">
                <h2>Párty a posezení</h2>
            </div>
            <div class="container">
                <div id="chatwindow">
                    <p class="message"></p>
                </div>
                <form action="pap.php" method="POST" class="<?php if ($db->userIsBanned($user->userLogged["username"]) or ($user->userLogged === null)) echo 'hidden'; else echo 'show';?>">
                    <input type="text" id="text" name="textMessage" autofocus autocomplete="off">
                    <button type="submit" name="submit">Odeslat</button>
                </form>
                <?php if(!$db->userExists($user->userLogged["username"])) { ?>
                    <div class="formerror">Nejste přihlášen.</div>
                <?php } else if($db->userIsBanned($user->userLogged["username"])) { ?>
                    <div class="formerror">Máte ban.</div>
                <?php } ?>
            </div>
            <div class="sidesmall">
                <div class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>">
                    <form action="pap.php" method="POST" id="tagDelForm">
                        <div>
                            <label for="tagDelete">Smazat zprávu:
                                <input type="text" name="tagDelete" id="tagDelete" autocomplete="off" required>
                            </label>
                            <?php if (!$messageExists) { ?>
                                <div class="formerror">Správa neexistuje.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <button type="submit" name="deleteMessage">Smazat</button>
                        </div>
                    </form>
                </div>
            </div> 
        </main>

        <footer>

        </footer>

        <script src="source/FELchat.js"></script>
        <script src="source/pap/chat.js"></script>
    
    </body>
</html>