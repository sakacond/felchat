<?php

require "source/app.php";

function escape($value) {
    echo htmlspecialchars($value, ENT_QUOTES);
}

$skin = isset($_COOKIE["skin"]);

$userPage = $_REQUEST["u"];
$adminPage = $_REQUEST["a"];

$rankUpFormSent = isset($_POST["rankUp"]);
$userExistsR = false;
$adminCheck = $db->adminExists($user->userLogged["username"]);

//on this page 'uzivatele.php' is 5 different forms, which all take the name the administrator sent and if it is in database they do:

//make user an administrator
if ($rankUpFormSent) {
    $username = $_POST["usernameR"];
    $formUser = $db->getUser($username);
    if ($formUser) {
        $userExistsR = true;
        if ($adminCheck){
            $db->addAdmin($_POST["usernameR"]);
            header("Location: uzivatele.php?u=0&a=0");
            exit();
        }
    }
}

$deleteFormSent = isset($_POST["delete"]);
$userExistsD = false;

//delete user account
if($deleteFormSent) {
    $username = $_POST["usernameD"];
    $formUser = $db->getUser($username);
    if($formUser) {
        $userExistsD = true;
        if ($adminCheck){
            $db->deleteUser($_POST["usernameD"]);
            header("Location: uzivatele.php?u=0&a=0");
            exit();
        }
    }
}

$adDeleteFormSent = isset($_POST["deleteAd"]);
$userExistsAdD = false;

//make administrator a regular user
if ($adDeleteFormSent) {
    $username = $_POST["usernameAdD"];
    $formUser = $db->getAdmin($username);
    if ($formUser) {
        $userExistsAdD = true;
        if ($adminCheck){
            $db->deleteAdmin($_POST["usernameAdD"]);
            header("Location: uzivatele.php?u=0&a=0");
            exit();
        }
    }
}

$banFormSent = isset($_POST["ban"]);
$userExistsB = false;
$userIsBanned = false;

//ban user
if ($banFormSent) {
    $username = $_POST["usernameB"];
    $formUser = $db->getUser($username);
    $userIsBanned = $db->userIsBanned($username);
    if ($formUser && !$userIsBanned) {
        $userExistsB = true;
        if ($adminCheck){
            $db->banUser($_POST["usernameB"]);
            header("Location: uzivatele.php?u=0&a=0");
            exit();
        }
    }
}

$unBanFormSent = isset($_POST["unban"]);
$userExistsU = false;

//un-ban user
if ($unBanFormSent) {
    $username = $_POST["usernameU"];
    $formUser = $db->userExists($username);
    $userIsBanned = $db->userIsBanned($username);
    if ($formUser && $userIsBanned) {
        $userExistsU = true;
        if ($adminCheck){
            $db->unBanUser($_POST["usernameU"]);
            header("Location: uzivatele.php?u=0&a=0");
            exit();
        }
    }
}

//all the forms and lists of users and admins are unavailbile to regular users, this is achieved by the $db->isAdmin() function that controls if the logged user is authorized to read or edit the user databases

?>

<!DOCTYPE html>
<html class="<?php if ($skin) echo 'day';?>" lang="cs">
    <head>
        <title>
            FELchat - Uživatelé
        </title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="source/felchat-style.css">
    </head>
    <body>
        <header>
            <a class="topbar" href="index.php"><h1>FELchat</h1></a>
            <ul class="<?php if ($user->userLogged !== null) echo 'toolbar'; else echo 'hidden';?>" id="links-left">    
                <li><a href="profil.php"><h1>
                    <?php
                    if ($user->userLogged === null) {
                        echo 'Not Empty Heading';
                    } else {
                        $name = $user->userLogged["username"]; 
                        if (strlen($name) <= 12) {
                            echo ($name);
                        } else {
                            $shrtName = substr($name, 0, 9);
                            echo ($shrtName); ?>...<?php }
                    }
                    ?></h1></a></li>
                <li class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>"><a href="uzivatele.php?u=0&a=0" class="active"><h1>Uživatelé</h1></a></li>
                <li><a href="logOut.php" id="pseudo-links-right"><h1>Odhlášení</h1></a></li> 
            </ul> 
            <ul class="<?php if ($user->userLogged !== null) echo 'hidden'; else echo 'toolbar';?>" id="links-right">   
                <li><a href="registrace.php"><h1>Registrace</h1></a></li>
                <li><a href="prihlaseni.php"><h1>Přihlášení</h1></a></li>
            </ul>
            <ul id="roombar" class="show">
                <li><h1>Místnosti:</h1></li>
                <li><a href="global.php"><h1>/ Globální chat /</h1></a></li>
                <li><a href="skola.php"><h1>/ Škola /</h1></a></li>
                <li><a href="gaming.php"><h1>/ Gaming /</h1></a></li>
                <li><a href="hobby.php"><h1>/ Hobby /</h1></a></li>
                <li><a href="pap.php"><h1>/ Párty a posezení /</h1></a></li>
            </ul>
        </header>
        <main>
            <div class="maininfo">
                <div id="users"> 
                    <div class="big">
                        <h2>Uživatelé</h2>
                    </div>
                    <?php 
                        //this function finds and echoes the 30 users on the page that the administrator is looking through right now
                        if ($adminCheck) { 
                            $i = 0;
                            foreach ($db->getUsers() as $user) { 
                                if (($userPage * 30) <= $i and ((($userPage * 30) - $i) >= 0)) { ?>
                        <div><?php if($db->userIsBanned($user["username"])) echo '<span class="ban">BANNED </span>'.$user["username"]; else echo $user["username"];?></div>
                    <?php }}} else { ?>
                        <div class="formerror">Nemáte administrátorské práva.</div>
                    <?php } ?>
                    <div class="paging">
                        <ul>
                            <!-- This is paginator for users, below is function for echoing 30 administrators and paginator for the administrator page -->
                            <li><a href="uzivatele.php?u=0&a=<?php echo $adminPage; ?>">&lt;&lt;</a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage - 1; ?>&a=<?php echo $adminPage; ?>" class="<?php if ($userPage <= 0) echo 'hidden'; else echo 'show'?>">&lt;</a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage - 2; ?>&a=<?php echo $adminPage; ?>" class="<?php if ($userPage - 1 <= 0) echo 'hidden'; else echo 'show'?>"><?php echo $userPage - 1?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage - 1; ?>&a=<?php echo $adminPage; ?>" class="<?php if ($userPage <= 0) echo 'hidden'; else echo 'show'?>"><?php echo $userPage?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage; ?>" class="active"><?php echo $userPage + 1?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage + 1; ?>&a=<?php echo $adminPage; ?>" class="<?php if ((($userPage + 1 ) * 30) >= $db->userCount()) echo 'hidden'; else echo 'show'?>"><?php echo $userPage + 2?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage + 2; ?>&a=<?php echo $adminPage; ?>" class="<?php if ((($userPage + 2 ) * 30) >= $db->userCount()) echo 'hidden'; else echo 'show'?>"><?php echo $userPage + 3?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage + 1; ?>&a=<?php echo $adminPage; ?>" class="<?php if ((($userPage + 1 ) * 30) >= $db->userCount()) echo 'hidden'; else echo 'show'?>">&gt;</a></li>
                            <li><a href="uzivatele.php?u=<?php echo (ceil($db->userCount() / 30) - 1); ?>&a=<?php echo $adminPage; ?>">&gt;&gt;</a></li>
                        </ul>
                    </div>
                </div>
                <div id="admins">
                    <div class="big">
                        <h2>Administrátoři</h2>
                    </div>
                    <?php 
                        $i = 0;
                        if ($adminCheck) { 
                            foreach ($db->getAdmins() as $admin) { 
                                if (($adminPage * 30) <= $i and ((($adminPage * 30) - $i) >= 0)) { ?>
                                    <div><?php echo $admin["username"] ?></div>
                    <?php }}} else { ?>
                        <div class="formerror">Nemáte administrátorské práva.</div>
                    <?php } ?>
                    <div class="paging">
                        <ul>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=0">&lt;&lt;</a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage - 1; ?>" class="<?php if ($adminPage <= 0) echo 'hidden'; else echo 'show'?>">&lt;</a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage - 2; ?>" class="<?php if ($adminPage - 1 <= 0) echo 'hidden'; else echo 'show'?>"><?php echo $adminPage - 1?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage - 1; ?>" class="<?php if ($adminPage <= 0) echo 'hidden'; else echo 'show'?>"><?php echo $adminPage?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage; ?>" class="active"><?php echo $adminPage + 1?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage + 1; ?>" class="<?php if ((($adminPage + 1 ) * 30) >= $db->adminCount()) echo 'hidden'; else echo 'show'?>"><?php echo $adminPage + 2?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage + 2; ?>" class="<?php if ((($adminPage + 2 ) * 30) >= $db->adminCount()) echo 'hidden'; else echo 'show'?>"><?php echo $adminPage + 3?></a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo $adminPage + 1; ?>" class="<?php if ((($adminPage + 1 ) * 30) >= $db->adminCount()) echo 'hidden'; else echo 'show'?>">&gt;</a></li>
                            <li><a href="uzivatele.php?u=<?php echo $userPage; ?>&a=<?php echo (ceil($db->adminCount() / 30) - 1); ?>">&gt;&gt;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="side">
                <form action="uzivatele.php?u=0&a=0" method="POST" id="formAd">
                    <fieldset>
                        <legend>Přidat administrátora:</legend>
                        <br>
                        <div>
                            <label for="usernameR">Přihlašovací jméno: 
                                <input id="usernameR" name="usernameR" type="text" value="<?php if (isset($_POST["usernameR"])) escape($_POST["usernameR"]) ?>" required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků.">
                                <?php if ($rankUpFormSent && !$userExistsR) { ?>
                                    <div class="formerror">Uživatel neexistuje.</div>
                                <?php } ?>
                                <?php if (!$adminCheck) { ?>
                                    <div class="formerror">Nemáte administrátorské práva.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <button name="rankUp" type="submit">Povýšit</button>
                    </fieldset>
                </form>
                <form action="uzivatele.php?u=0&a=0" method="POST" id="formAdDel">
                    <fieldset>
                        <legend>Odebrat práva:</legend>
                        <br>
                        <div>
                            <label for="usernameAdD">Přihlašovací jméno: 
                                <input id="usernameAdD" name="usernameAdD" type="text" value="<?php if (isset($_POST["usernameAdD"])) escape($_POST["usernameAdD"]) ?>" required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků.">
                                <?php if ($adDeleteFormSent && !$userExistsAdD) { ?>
                                    <div class="formerror">Uživatel není administrátorem.</div>
                                <?php } ?>
                                <?php if (!$adminCheck) { ?>
                                    <div class="formerror">Nemáte administrátorské práva.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <button name="deleteAd" type="submit">Odebrat</button>
                    </fieldset>
                </form>
                <form action="uzivatele.php?u=0&a=0" method="POST" id="formBan">
                    <fieldset>
                        <legend>Udělit ban:</legend>
                        <br>
                        <div>
                            <label for="usernameB">Přihlašovací jméno: 
                                <input id="usernameB" name="usernameB" type="text" value="<?php if (isset($_POST["usernameD"])) escape($_POST["usernameD"]) ?>" required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků.">
                                <?php if ($banFormSent && $userIsBanned) { ?>
                                    <div class="formerror">Uživatel už má ban.</div>
                                <?php } else if ($banFormSent && !$userExistsB) { ?>
                                    <div class="formerror">Uživatel neexistuje.</div>
                                <?php } ?>
                                <?php if (!$adminCheck) { ?>
                                    <div class="formerror">Nemáte administrátorské práva.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <button name="ban" type="submit">Ban</button>
                    </fieldset>
                </form>
                <form action="uzivatele.php?u=0&a=0" method="POST" id="formUnBan">
                    <fieldset>
                        <legend>Odstranit ban:</legend>
                        <br>
                        <div>
                            <label for="usernameU">Přihlašovací jméno: 
                                <input id="usernameU" name="usernameU" type="text" value="<?php if (isset($_POST["usernameD"])) escape($_POST["usernameD"]) ?>" required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků.">
                                <?php if ($unBanFormSent && !$userIsBanned) { ?>
                                    <div class="formerror">Uživatel nemá ban.</div>
                                <?php } else if($unBanFormSent && !$userExistsU) { ?>
                                    <div class="formerror">Uživatel neexistuje</div>
                                <?php } ?>
                                <?php if (!$adminCheck) { ?>
                                    <div class="formerror">Nemáte administrátorské práva.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <button name="unban" type="submit">Unban</button>
                    </fieldset>
                </form>
                <form action="uzivatele.php?u=0&a=0" method="POST" id="formDelUser">
                    <fieldset>
                        <legend>Vymazat účet:</legend>
                        <br>
                        <div>
                            <label for="usernameD">Přihlašovací jméno: 
                                <input id="usernameD" name="usernameD" type="text" value="<?php if (isset($_POST["usernameD"])) escape($_POST["usernameD"]) ?>" required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků.">
                                <?php if ($deleteFormSent && !$userExistsD) { ?>
                                    <div class="formerror">Uživatel neexistuje.</div>
                                <?php } ?>
                                <?php if (!$adminCheck) { ?>
                                    <div class="formerror">Nemáte administrátorské práva.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <button name="delete" type="submit">Smazat</button>
                    </fieldset>
                </form>
            </div>
        </main>
        <script src="source/FELchat.js"></script>
    </body>
</html>
