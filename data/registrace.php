<?php

require "source/app.php";

function escape($value) {
    echo htmlspecialchars($value, ENT_QUOTES);
}

$skin = isset($_COOKIE["skin"]);

$formSent = !empty($_POST);

$nameValid = false;
$nameAvaliable = false;
$passValid = false;
$passSame = false;
$emailAvailiable = false;
$emailValid = false;

//php validator of the registration form
if ($formSent) {
    $nameValid = isset($_POST["username"]) && strlen($_POST["username"]) >= 6 && strlen($_POST["username"]) <= 30;
    $nameAvaliable = isset($_POST["username"]) && !$db->userExists($_POST["username"]);
    $emailValid = isset($_POST["email"]) && strpos($_POST["email"], "@");
    $emailAvailiable = isset($_POST["email"]) && !$db->emailUsed($_POST["email"]);
    $passValid = isset($_POST["pass"]) && strlen($_POST["pass"]) >= 8 && strlen($_POST["pass"]) <= 30;
    $passSame = isset($_POST["pass"]) && isset($_POST["passcheck"]) && $_POST["pass"] === $_POST["passcheck"];

    $formValid = $nameValid && $nameAvaliable && $passValid && $passSame && $emailAvailiable && $emailValid;

    if ($formValid) {
        $db->addUser($_POST["username"], $_POST["email"], $_POST["pass"]);
        $user->logIn($_POST["username"]);
        header("Location: index.php");
        exit();
    }
}

?>

<!DOCTYPE html>
<html class="<?php if ($skin) echo 'day';?>" lang="cs">
    <head>
        <title>
            FELchat - Registrace
        </title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="source/felchat-style.css">
    </head>
    <body>
        <header>
            <a class="topbar" href="index.php"><h1>FELchat</h1></a>
            <ul class="<?php if ($user->userLogged !== null) echo 'toolbar'; else echo 'hidden';?>" id="links-left">    
                <li><a href="profil.php"><h1>
                    <?php
                    if ($user->userLogged === null) {
                        echo 'Not Empty Heading';
                    } else {
                        $name = $user->userLogged["username"]; 
                        if (strlen($name) <= 12) {
                            echo ($name);
                        } else {
                            $shrtName = substr($name, 0, 9);
                            echo ($shrtName); ?>...<?php }
                    }
                    ?></h1></a></li>
                <li class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>"><a href="uzivatele.php?u=0&a=0"><h1>Uživatelé</h1></a></li>
                <li><a href="logOut.php" id="pseudo-links-right"><h1>Odhlášení</h1></a></li> 
            </ul> 
            <ul class="<?php if ($user->userLogged !== null) echo 'hidden'; else echo 'toolbar';?>" id="links-right">   
                <li><a href="registrace.php" class="active"><h1>Registrace</h1></a></li>
                <li><a href="prihlaseni.php"><h1>Přihlášení</h1></a></li>
            </ul>
            <ul id="roombar" class="show">
                <li><h1>Místnosti:</h1></li>
                <li><a href="global.php"><h1>/ Globální chat /</h1></a></li>
                <li><a href="skola.php"><h1>/ Škola /</h1></a></li>
                <li><a href="gaming.php"><h1>/ Gaming /</h1></a></li>
                <li><a href="hobby.php"><h1>/ Hobby /</h1></a></li>
                <li><a href="pap.php"><h1>/ Párty a posezení /</h1></a></li>
            </ul>
        </header>
        <main>
            <div class="big">
                <h2>Vítejte na FELchatu!</h2>
                <h3>Chat pro všechny správné FELáky.</h3> 
            </div>
            <div class="maininfo">
                <h2>Co je to FELchat?</h2>
                <h3>Virtuální místo, kde se můžou scházet všichni FELáci, bez ohledu na věk, titul, katedru nebo obor. Můžou spolu sdílet své zkušenosti, zábavné i smutné historky a pomáhat si navzájem.</h3>
                <h2>Proč používat FELchat, když mám facebook?</h2>
                <h3>
                    Především anonymita ale taky to, že je to lightweight klient využívající servery ČVUT. Nemusíte se bát, že Mark si bude koukat,
                    které profesory nemáte rádi, nebo kteří/é spolužáci/čky se vám líbí. Diskuze se všemi o všem. Technické i sociální. Všechno na jednom místě.
                </h3>
                <h2>Kto je FELák?</h2>
                <h3>
                    Odpověď na tuto otázku není jednoduchá, avšak dá se s jistotou tvrdit, že každý FELák je přátelský student nebo přátel studentů ČVUT. Jestli jsi si pořád není jistý, jestli jsi správnej FELák,
                    můžeš si kouknout video, které ti možná osvětlí, co to to FELáctví vlastně je. >>>
                </h3>
            </div>
            <div class="side">
                <div class="big">
                    <h2>Chcete se stát členem?</h2>
                    <h3>Jednoduše vyplňte registrační formulář níže.</h3> 
                </div>
                <form action="registrace.php" method="POST" id="formReg">
                    <fieldset>
                        <legend>Registrační formulář</legend>
                        <br>
                        <div>
                            <label>Přihlašovací jméno: <input id="username" name="username" type="text" value="<?php if (isset($_POST["username"])) escape($_POST["username"]) ?>" autofocus required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků."></label>
                            <?php if ($formSent && !$nameValid) { ?>
                                <div class="formerror">Přihlašovací jméno musí mít alespoň 6 - 30 znaků.</div>
                            <?php } else if ($formSent && !$nameAvaliable) { ?>
                                <div class="formerror">Přihlašovací jméno už existuje. Zvolte prosím jiné.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <label>E-mail: <input id="email" name="email" type="email" value="<?php if (isset($_POST["email"])) escape($_POST["email"]) ?>" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"></label>
                            <?php if ($formSent && !$emailValid) { ?>
                                <div class="formerror">E-mail byl nesprávně zadán.</div>
                            <?php } else if ($formSent && !$emailAvailiable) { ?>
                                <div class="formerror">E-mail už byl použit. Použijte prosím jiný.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <label>Heslo: <input id="pass" name="pass" type="password" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($formSent && !$passValid) { ?>
                                <div class="formerror">Heslo musí mít alespoň 8 - 30 znaků.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <label>Heslo znovu: <input id="passcheck" name="passcheck" type="password" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($formSent && !$passSame) { ?>
                                <div class="formerror">Hesla se nezhodují.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <button id="submit" type="submit">Odeslat</button>
                        </div>
                    </fieldset>
                </form>        
            </div>
        </main>
        <footer>
            
        </footer>
        
        <script src="source/FELchat.js"></script>

    </body>
</html>