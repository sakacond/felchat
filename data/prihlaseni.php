<?php

require "source/app.php";

function escape($value) {
    echo htmlspecialchars($value, ENT_QUOTES);
}

$skin = isset($_COOKIE["skin"]);

$formSent = !empty($_POST);

$nameExists = false;
$passChecked = false;

//php validator of the login form
if ($formSent) {
    $username = $_POST["username"];

    $formUser = $db->getUser($username);
    if ($formUser) {
        $nameExists = true;
        $passCheck = password_verify($_POST["password"], $formUser["password"]);
        
        if ($nameExists && $passCheck) {
            $user->logIn($_POST["username"]);
            header("Location: index.php");
            exit();
        }
    }
}

?>

<!DOCTYPE html>
<html class="<?php if ($skin) echo 'day';?>" lang="cs">
    <head>
        <title>
            FELchat - Přihlášení
        </title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="source/felchat-style.css">
    </head>
    <body>
        <header>
            <a class="topbar" href="index.php"><h1>FELchat</h1></a>
            <ul class="<?php if ($user->userLogged !== null) echo 'toolbar'; else echo 'hidden';?>" id="links-left">    
                <li><a href="profil.php"><h1>
                    <?php
                    if ($user->userLogged === null) {
                        echo 'Not Empty Heading';
                    } else {
                        $name = $user->userLogged["username"]; 
                        if (strlen($name) <= 12) {
                            echo ($name);
                        } else {
                            $shrtName = substr($name, 0, 9);
                            echo ($shrtName); ?>...<?php }
                    }
                    ?></h1></a></li>
                <li class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>"><a href="uzivatele.php?u=0&a=0"><h1>Uživatelé</h1></a></li>
                <li><a href="logOut.php" id="pseudo-links-right"><h1>Odhlášení</h1></a></li> 
            </ul> 
            <ul class="<?php if ($user->userLogged !== null) echo 'hidden'; else echo 'toolbar';?>" id="links-right">   
                <li><a href="registrace.php"><h1>Registrace</h1></a></li>
                <li><a href="prihlaseni.php" class="active"><h1>Přihlášení</h1></a></li>
            </ul>
            <ul id="roombar" class="show">
                <li><h1>Místnosti:</h1></li>
                <li><a href="global.php"><h1>/ Globální chat /</h1></a></li>
                <li><a href="skola.php"><h1>/ Škola /</h1></a></li>
                <li><a href="gaming.php"><h1>/ Gaming /</h1></a></li>
                <li><a href="hobby.php"><h1>/ Hobby /</h1></a></li>
                <li><a href="pap.php"><h1>/ Párty a posezení /</h1></a></li>
            </ul>
        </header>
        <main>
            <div class="big">
                <h2>Vítejte na FELchatu!</h2>
                <h3>Chat pro všechny správné FELáky.</h3> 
            </div>
            <div class="maininfo">
                <h2>Co je to FELchat?</h2>
                <h3>Virtuální místo, kde se můžou scházet všichni FELáci, bez ohledu na věk, titul, katedru nebo obor. Můžou spolu sdílet své zkušenosti, zábavné i smutné historky a pomáhat si navzájem.</h3>
                <h2>Proč používat FELchat, když mám facebook?</h2>
                <h3>
                    Především anonymita ale taky to, že je to lightweight klient využívající servery ČVUT. Nemusíte se bát, že Mark si bude koukat,
                    které profesory nemáte rádi, nebo kteří/é spolužáci/čky se vám líbí. Diskuze se všemi o všem. Technické i sociální. Všechno na jednom místě.
                </h3>
                <h2>Kto je FELák?</h2>
                <h3>
                    Odpověď na tuto otázku není jednoduchá, avšak dá se s jistotou tvrdit, že každý FELák je přátelský student nebo přátel studentů ČVUT. Jestli jsi si pořád není jistý, jestli jsi správnej FELák,
                    můžeš si kouknout video, které ti možná osvětlí, co to to FELáctví vlastně je. >>>
                </h3>
            </div>
            <div class="side">
                <div class="big">    
                    <h2>Už máte účet?</h2>
                    <h3>Přihlašte se zde.</h3>
                </div>
                <form action="prihlaseni.php" method="POST" id="formLog">
                    <fieldset>
                        <legend>Přihlášení</legend>
                        <br>
                        <div>
                            <label for="username">Přihlašovací jméno: 
                                <input id="username" name="username" type="text" value="<?php if (isset($_POST["username"])) escape($_POST["username"]) ?>" autofocus required pattern=".{6,30}" title="Jméno musí mít 6 - 30 znaků.">
                                <?php if ($formSent && !$nameExists) { ?>
                                    <div class="formerror">Neexistující přihlašovací jméno.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <div>
                            <label for="password">Heslo: 
                                <input id="password" name="password" type="password"  value="<?php if (isset($_POST["password"])) escape($_POST["password"]) ?>" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků.">
                                <?php if ($formSent && $nameExists && !$passChecked) { ?>
                                    <div class="formerror">Nesprávné heslo. Skuste znovu.</div>
                                <?php } ?>
                            </label>
                        </div>
                        <br>
                        <button id="submit" type="submit">Odeslat</button>
                    </fieldset>
                </form>
            </div> 
        </main>
        <footer>
            
        </footer>

        <script src="source/FELchat.js"></script>

    </body>
</html>