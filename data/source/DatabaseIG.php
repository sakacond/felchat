<?php

class DB {
    private $data;
    private $aData;
    private $bData;

    public function __construct() { //constructor loads all 3 databases, database of users, admins and banned users
        $this->loadDb();
        $this->loadAdb();
        $this->loadB();
    }

    //USER FUNCTIONS

    public function userExists($username) { //checks if user exists in database
        $user = $this->getUser($username);
		return (boolean)$user;
    }

    public function getUsers() {    //gets users from database
		return $this->data["users"];
	}

    public function emailUsed($email) { //checks if email is already used in database
        foreach ($this->data["users"] as $user) {
            if ($user["email"] === $email) {
                return true;
            }
        }
        return false;
    }

    public function addUser($username, $email, $password) { //adds new user if he doesn't already exist in database, else it throws an exception, this is also checked at sending registration form, so it should never throw the exception
        if ($this->userExists($username))  {
			throw new Exception("User already exists");
		}

        array_push($this->data["users"], [
            "username" => $username,
            "email" => $email,
            "password" => password_hash($password, PASSWORD_DEFAULT),
            "color" => "rgb(0, 255, 0)"
        ]);
        $this->saveDb();
    }

    public function reReg($username, $email, $password, $color) {   //re-registration of user used for changing email adress (more in implementation)
        if ($this->userExists($username))  {
			throw new Exception("User already exists");
		}

        array_push($this->data["users"], [
            "username" => $username,
            "email" => $email,
            "password" => $password,
            "color" => $color
        ]);
        
        $this->saveDb();
    }

    public function changeColor($username, $color) {    //setting custom color in chat, the default one is green "rgb(0, 255, 0)"
        $isAdmin = $this->adminExists($username);
        $newReg = $this->getUser($username);
        $this->deleteUser($username);
        $this->reReg($newReg["username"], $newReg["email"], $newReg["password"], $color);
        if ($isAdmin) {
            $this->addAdmin($username);
        }
    }

	public function getUser($username) {    //gets particular user from database
		foreach ($this->data["users"] as $user) {
			if ($user["username"] === $username) {
				return $user;
			}
		}
		return null;
	}

    public function passCheck($username, $password) {   //veryfies if the password that has been sent from client is the same as the one asociated with username the client claims to be his 
        foreach ($this->data["users"] as $user) {
            if ($user["username"] === $username && password_verify($_POST["password"], $user["password"])) {
                return true;
            }
        }
        return false;
    }

    public function changeEmail($username, $email) {    //algorythm for changing email of already registered user (more in implementation)
        $isAdmin = $this->adminExists($username);
        $newReg = $this->getUser($username);
        $this->deleteUser($username);
        $this->reReg($newReg["username"], $email, $newReg["password"], $newReg["color"]);
        if ($isAdmin) {
            $this->addAdmin($username);
        }
    }

    public function changePassword($username, $pass) {  //algorythm for changing password of already registered user (more in implementation)
        $isAdmin = $this->adminExists($username);
        $newReg = $this->getUser($username);
        $this->deleteUser($username);
        $this->addUser($newReg["username"], $newReg["email"], $pass, $newReg["color"]);
        if ($isAdmin) {
            $this->addAdmin($username);
        }
    }

    public function deleteUser($username) { //this function alows you to delete your own account
        $count = 0;
        foreach ($this->data["users"] as $user) {
            if($user["username"] === $username) {
                $isAdmin = $this->adminExists($username);
                array_splice($this->data["users"], $count, 1);
                if ($isAdmin) {
                    $this->deleteAdmin($username);
                }
                break;
            }
            $count = $count + 1;
        }

        $this->saveDb();
    }
    
    public function userCount() {   //count's all users in database, it is used for their listing in page /uzivatele.php that uses pagination
        $i = 0;
        foreach($this->data["users"] as $user) {
            $i = $i + 1;
        }
        return $i;
    }

    //ADMIN FUNCTIONS - basicly the same as User functions, just for the database of administrators

    public function deleteAdmin($username) {
        $count = 0;
        foreach ($this->aData["admins"] as $admin) {
            if($admin["username"] === $username) {
                array_splice($this->aData["admins"], $count, 1);
                break;
            }
            $count = $count + 1;
        }

        $this->saveAdb();
    }
    
    public function getAdmin($username) {
		foreach ($this->aData["admins"] as $admin) {
			if ($admin["username"] === $username) {
				return $admin;
			}
		}
		return null;
    }

    public function getAdmins() {
		return $this->aData["admins"];
	}
    
    public function adminExists($username) {
        $admin = $this->getAdmin($username);
		return (boolean)$admin;
    }

    public function addAdmin($username) {
        if ($this->adminExists($username))  {
			throw new Exception("Admin already exists");
		}

        array_push($this->aData["admins"], [
            "username" => $username
        ]);

        $this->saveAdb();
    }

    public function adminCount() {
        $i = 0;
        foreach($this->aData["admins"] as $admin) {
            $i = $i + 1;
        }
        return $i;
    }

    //BAN SYSTEM FUNCTIONS - the same for these functions, they only work with the ban database that is structured exactly like the database of administrators

    public function unBanUser($username) {
        $count = 0;
        foreach ($this->bData["bans"] as $ban) {
            if($ban["username"] === $username) {
                array_splice($this->bData["bans"], $count, 1);
                break;
            }
            $count = $count + 1;
        }

        $this->saveB();
    }
    
    public function getBan($username) {
		foreach ($this->bData["bans"] as $ban) {
			if ($ban["username"] === $username) {
				return $ban;
			}
		}
		return null;
    }

    public function getABans() {
		return $this->bData["bans"];
	}
    
    public function userIsBanned($username) {
        $ban = $this->getBan($username);
		return (boolean)$ban;
    }

    public function banUser($username) {
        if ($this->userIsBanned($username))  {
			throw new Exception("User is already banned");
		}

        array_push($this->bData["bans"], [
            "username" => $username
        ]);

        $this->saveB();
    }

    //DATABASE HANDLING FUNCTIONS
 
    private function loadDb() { //loads database of users
        $fileContent = trim(file_get_contents("Database.txt", true));
		if (!$fileContent) {
			$this->data = [
				"users" => []
			];
		} else {
			$this->data = json_decode($fileContent, true);
		}
    }

    private function loadAdb() {    //loads database of administrators
        $fileContent = trim(file_get_contents("adminDatabase.txt", true));
		if (!$fileContent) {
			$this->aData = [
				"admins" => []
			];
		} else {
			$this->aData = json_decode($fileContent, true);
		}
    }

    private function loadB() {  //loads database of banned users
        $fileContent = trim(file_get_contents("bans.txt", true));
		if (!$fileContent) {
			$this->bData = [
				"bans" => []
			];
		} else {
			$this->bData = json_decode($fileContent, true);
		}
    }

    //the last three functions are saving new data to databases

    private function saveAdb() {    
        file_put_contents("adminDatabase.txt", json_encode($this->aData), FILE_USE_INCLUDE_PATH);
    }

    private function saveDb() {
        file_put_contents("Database.txt", json_encode($this->data), FILE_USE_INCLUDE_PATH);
    }

    private function saveB() {
        file_put_contents("bans.txt", json_encode($this->bData), FILE_USE_INCLUDE_PATH);
    }
}