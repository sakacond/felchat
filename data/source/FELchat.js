//*REGISTRATION FORM VALIDATION*

var formReg = document.querySelector("#formReg");

if (!(formReg === null)) {
    formReg.addEventListener("submit", function(event) {
        console.log(event);
        
        formValid = validateRegForm();
        
        if (!formValid) {
            event.preventDefault();
        }
    });
}

function validateRegForm() {
    var username = document.querySelector("#username");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    var email = document.querySelector("#email");
    var emailValue = email.value;

    if (emailValue.length === 0){
        alert("Vyplňte emailovou adresu.");
        return false;
    }
    if (emailValue.length > 0) {
        if (!emailValue.includes("@")) {
            alert("Špatný formát emailové adresy.");
            return false;
        }
    }

    var pass = document.querySelector("#pass");
    var password = pass.value;
    var passcheck = document.querySelector("#passcheck");
    var passwordCheck = passcheck.value;
    
    if (password.length === 0) {
        alert("Vyplňte heslo.");
        return false;
    }
    if (password.length < 8) {
        alert("Heslo je příliš krátké. Minimum je 8 znaků.");
        return false;
    }
    if (password.length > 30) {
        alert("Heslo je příliš dlouhé. Maximum je 30 znaků.");
        return false;
    }
    if (8 <= password.length <= 30) {
        if (!(password === passwordCheck)) {
            alert("Kontrola hesla byla neůspěšná.");
            return false;
        }
    }

    return true;
}

//*LOGIN FORM VALIDATION*

var formLog = document.querySelector("#formLog");

if (!(formLog === null)) {
    formLog.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateLogForm();
        
        if (!formValid) {
            event.preventDefault();
        }
    });
}

function validateLogForm() {
    var username = document.querySelector("#username");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    var pass = document.querySelector("#password");
    var password = pass.value;
    
    if (password.length === 0) {
        alert("Vyplňte heslo.");
        return false;
    }
    if (password.length < 8) {
        alert("Heslo je příliš krátké. Minimum je 8 znaků.");
        return false;
    }
    if (password.length > 30) {
        alert("Heslo je příliš dlouhé. Maximum je 30 znaků.");
        return false;
    }
    
    return true;
}

//*PROFILE CUSTOMIZATION VALIDATIONS*

//email change

var emailChangeForm = document.querySelector("#emailCh");

if(!(emailChangeForm === null)) {
    emailChangeForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateEmailChange();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateEmailChange() {
    var email = document.querySelector("#email");
    var pass = document.querySelector("#pass");
    var emailValue = email.value;
    var password = pass.value;

    if (emailValue.length === 0){
        alert("Vyplňte emailovou adresu.");
        return false;
    }
    if (emailValue.length > 0) {
        if (!emailValue.includes("@")) {
            alert("Špatný formát emailové adresy.");
            return false;
        }
    }

    if (password.length === 0) {
        alert("Vyplňte heslo.");
        return false;
    }
    if (password.length < 8) {
        alert("Heslo je příliš krátké. Minimum je 8 znaků.");
        return false;
    }
    if (password.length > 30) {
        alert("Heslo je příliš dlouhé. Maximum je 30 znaků.");
        return false;
    }

    return true;
}

//password change

var passwordChangeForm = document.querySelector("#passCh");

if(!(passwordChangeForm === null)) {
    passwordChangeForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validatePasswordChange();
        if(!formValid) {
            event.preventDefault();
        }
    })
}

function validatePasswordChange() {
    var oldPass = document.querySelector("#oldPass");
    var oldPassword = oldPass.value;
    var newPass = document.querySelector("#newPass");
    var newPassword = newPass.value;
    var newPassCheck = document.querySelector("#passcheck");
    var newPasswordCheck = newPassCheck.value;

    if (oldPassword.length === 0) {
        alert("Vyplňte staré heslo.");
        return false;
    }
    if (oldPassword.length < 8) {
        alert("Staré heslo je příliš krátké. Minimum je 8 znaků.");
        return false;
    }
    if (oldPassword.length > 30) {
        alert("Staré heslo je příliš dlouhé. Maximum je 30 znaků.");
        return false;
    }

    if (newPassword.length === 0) {
        alert("Vyplňte nové heslo.");
        return false;
    }
    if (newPassword.length < 8) {
        alert("Nové heslo je příliš krátké. Minimum je 8 znaků.");
        return false;
    }
    if (newPassword.length > 30) {
        alert("Nové heslo je příliš dlouhé. Maximum je 30 znaků.");
        return false;
    }
    if (8 <= newPassword.length <= 30) {
        if (!(newPassword === newPasswordCheck)) {
            alert("Kontrola nového hesla byla neůspěšná.");
            return false;
        }
    }

    return true;
}

//delete account

var deleteOwnProfile = document.querySelector("#formDel");

if (!(deleteOwnProfile === null)) {
    deleteOwnProfile.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateDeleteOwnProfile();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateDeleteOwnProfile() {
    var pass = document.querySelector("#password");
    var password = pass.value;

    if (password.length === 0) {
        alert("Vyplňte heslo.");
        return false;
    }
    if (password.length < 8) {
        alert("Heslo je příliš krátké. Minimum je 8 znaků.");
        return false;
    }
    if (password.length > 30) {
        alert("Heslo je příliš dlouhé. Maximum je 30 znaků.");
        return false;
    }

    return true;
}

//*USER MANAGEMENT FORMS*

//promoting regular user to administrator form validation

var addAdminForm = document.querySelector("#formAd");

if (!(addAdminForm === null)) {
    addAdminForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateAddAdminForm();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateAddAdminForm() {
    var username = document.querySelector("#usernameR");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    return true;
}

//demoting administrator to regular user form validation

var deleteAdminForm = document.querySelector("#formAdDel");

if (!(deleteAdminForm === null)) {
    deleteAdminForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateDeleteAdminForm();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateDeleteAdminForm() {
    var username = document.querySelector("#usernameAdD");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    return true;
}

//ban user form validation

var banUserForm = document.querySelector("#formBan");

if (!(banUserForm === null)) {
    banUserForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateBanUserForm();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateBanUserForm() {
    var username = document.querySelector("#usernameB");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    return true;
}

//unban user form validation

var unBanUserForm = document.querySelector("#formUnBan");

if (!(unBanUserForm === null)) {
    unBanUserForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateUnBanUserForm();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateUnBanUserForm() {
    var username = document.querySelector("#usernameU");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    return true;
}

//delete user account form validation

var deleteUserForm = document.querySelector("#formDelUser");

if (!(deleteUserForm === null)) {
    deleteUserForm.addEventListener("submit", function(event) {
        console.log(event);

        var formValid = validateDeleteUserForm();
        if (!formValid) {
            event.preventDefault();
        }
    })
}

function validateDeleteUserForm() {
    var username = document.querySelector("#usernameD");
    var usernameValue = username.value;

    if (usernameValue.length === 0) {
        alert("Vyplňte přihlašovací jméno.");
        return false;
    }
    if (usernameValue.length < 6) {
        alert("Přihlašovací jméno je moc krátké. Minimum je 6 znaků.");
        return false;
    }
    if (usernameValue.length > 30) {
        alert("Přihlašovací jméno je příliš dlouhé. Maximum je 30 znaků.")
        return false;
    }

    return true;
}