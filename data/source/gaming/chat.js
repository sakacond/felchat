//request
var lastTag = "defaultTag";
var refreshTimer = setInterval(requestMessages, 5000);
var myDiv = document.getElementById("chatwindow");

function requestMessages() {
    var tag = JSON.stringify(lastTag);

    if (window.XMLHttpRequest) {
        var request = new XMLHttpRequest();
    } else {
        var request = new ActiveXObject("source/gaming/reqHand.php");
    }

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var messages = JSON.parse(this.responseText);
            if (messages.length <= 1) {
                return;
            }
            writeMessages(messages);
            myDiv.scrollTop = myDiv.scrollHeight;
        }
    };
    request.open("POST", "source/gaming/reqHand.php", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send("tag=" + tag);
}

function writeMessages(messages) {
    var i = 0;
    var len = messages.length;
    setGlobal(messages[len - 1].tag);
    while (i < len) {
        var message = messages[i];
        writeMessage(message.tag, message.time, message.color ,message.username, message.text);
        i = i + 1;
    }
}

function writeMessage(tag, time, colour, user, text) {
    var message = '<p><span class="tag">' + tag + '</span> ' + time + ' <span style="color:'+ colour +';">' + user + '</span>: ' + text + '</p>';

    document.getElementById("chatwindow").insertAdjacentHTML("beforeend", message);
}

function setGlobal(newTag) {
    lastTag = newTag;
}

requestMessages();