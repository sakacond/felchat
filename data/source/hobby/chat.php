<?php 

class ChatH{

    private $history;
    private $db;

    public function __construct($db) {
        $this->loadHis();
        $this->db = $db;
    }

    public function newMessage($time, $username, $text) {

        //deleting old messages to save up space in database
        while (count($this->history["messages"]) > 100) { 
            array_splice($this->history["messages"], 0, 1);
        }

        $tagSeed = $time . $username . $text;
        $tag = password_hash($tagSeed, PASSWORD_DEFAULT);

        //function makes sure that the tag is unique
        while($this->messageExists($tag)) {  
            $tagSeed = $tagSeed . $time;
            $tag = password_hash($tagSeed, PASSWORD_DEFAULT);
        }
        
        $user = $this->db->getUser($username);
        $color = $user["color"];

        array_push($this->history["messages"], [
            "tag" => $tag,
            "time" => $time,
            "color" => $color,
            "username" => $username,
            "text" => $text
        ]);

        $this->saveHis();
    }

    public function messageExists($tag) {
        $count = 0;
        foreach ($this->history["messages"] as $messages) {
            if ($messages["tag"] === $tag) {
                return true;
            }
            $count = $count + 1;
        }
        
        return false;
    }

    public function sendNewMessages($tag) {
        if($this->messageExists($tag)) {
            $i = 0;
            foreach ($this->history["messages"] as $messages) {
                if ($messages["tag"] === $tag) {
                    break;
                }
                $i = $i + 1;
            }
            if($i === count($this->history["messages"])) {
                echo json_encode("false");
            } 
            else {
                echo json_encode(array_splice($this->history["messages"], ($i - count($this->history["messages"]))));
            }
        } 
        else {
            echo json_encode($this->history["messages"]);
        }
    }

    public function deleteMessage($tag) {
        if ($this->messageExists($tag)) {
            $count = 0;
            foreach ($this->history["messages"] as $messages) {
                if($messages["tag"] === $tag) {
                    array_splice($this->history["messages"], $count, 1);
                }
                $count = $count + 1;
            }
        }

        $this->saveHis();
    }

    private function loadHis() {
        $fileContent = trim(file_get_contents("messages.txt", true));
		if (!$fileContent) {
			$this->history = [
				"messages" => []
			];
		} else {
			$this->history = json_decode($fileContent, true);
		}
    }

    private function saveHis() {
        file_put_contents("messages.txt", json_encode($this->history), FILE_USE_INCLUDE_PATH);
    }

}

?>