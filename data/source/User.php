<?php 

class User{
    
    public $userLogged;
    private $db;

    public function __construct($db) {  //if this constructor finds the username that is supposed to be logged in through the session in database it "logs him" in
        $this->db = $db;

        $sessionUser = isset($_SESSION["username"]);
        if ($sessionUser) {
            $this->logIn($_SESSION["username"]);
        }
    }
    
    public function logIn($username) {  //allows user to use his data if he is logged in
        $_SESSION["username"] = $username;
        $userData = $this->db->getUser($username);
        $this->userLogged = $userData;
    }
    
    public function logOut() {  //logs out user
        unset($_SESSION["username"]);
        $this->userLogged = null;
    }
    
    public function userLogged() {  //checks if user is logged in
        return (boolean)$this->userLogged;
    }
}

?>