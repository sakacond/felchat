<?php

//this app.php page links all the chat databases, user databases together with User.php that is handling sessions and checking if user is logged in and is authorized to do specific functions

session_start();

require "DatabaseIG.php";
require "User.php";
require "global/chat.php";
require "gaming/chat.php";
require "hobby/chat.php";
require "pap/chat.php";
require "skola/chat.php";

function write($value) {
    echo htmlspecialchars($value, ENT_QUOTES);
}

$db = new DB();
$user = new User($db);
$messageG = new ChatG($db);
$messageGa = new ChatGa($db);
$messageS = new ChatS($db);
$messageH = new ChatH($db);
$messageP = new ChatP($db);

?>