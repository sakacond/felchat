<?php 

class ChatG{

    private $history;
    public $newMessages;
    private $db;

    public function __construct($db) {
        $this->loadHis();
        $this->db = $db;
    }

    public function newMessage($time, $username, $text) {   //function for adding new messages to database

        //function is deleting old messages to save up space in database
        while (count($this->history["messages"]) > 100) { 
            array_splice($this->history["messages"], 0, 1);
        }

        $tagSeed = $time . $username . $text;
        $tag = password_hash($tagSeed, PASSWORD_DEFAULT);

        //function tags each message for their differentiation in database and makes sure that the tag is unique
        while($this->messageExists($tag)) {  
            $tagSeed = $tagSeed . $time;
            $tag = password_hash($tagSeed, PASSWORD_DEFAULT);
        }
        
        $user = $this->db->getUser($username);  //this is how function assigns current user color to his message from his database info where the color is stored
        $color = $user["color"];

        array_push($this->history["messages"], [
            "tag" => $tag,
            "time" => $time,
            "color" => $color,
            "username" => $username,
            "text" => $text
        ]);

        $this->saveHis();
    }

    public function messageExists($tag) {   //function checks out if the message tag is used in database, used for making sure each tag is unique, and also for validating the form for deleting messages
        $count = 0;
        foreach ($this->history["messages"] as $messages) {
            if ($messages["tag"] === $tag) {
                return true;
            }
            $count = $count + 1;
        }
        
        return false;
    }

    public function sendNewMessages($tag) { //this function is reacting on reqHand.php's requests for new messages (more in manual) and sends them based on the tag of the last recieved message in clients computer 
        if($this->messageExists($tag)) {
            $i = 0;
            foreach ($this->history["messages"] as $messages) {
                if ($messages["tag"] === $tag) {
                    break;
                }
                $i = $i + 1;
            }
            if($i === count($this->history["messages"])) {  //if there are no new messages it echoes the message "false"
                echo json_encode("false");
            } 
            else {
                echo json_encode(array_splice($this->history["messages"], ($i - count($this->history["messages"])))); //if there are newer messages than the last tag user sent it splices the array of messages in database and sends the new messages to reqHand.php
            }
        } 
        else {
            echo json_encode($this->history["messages"]);   //else, if user did not provide tag or the tag is not in database the function sends the whole message history
        }
    }

    public function deleteMessage($tag) { //algorythm that locates messages based on the tag it got and deletes them from database
        if ($this->messageExists($tag)) {
            $count = 0;
            foreach ($this->history["messages"] as $messages) {
                if($messages["tag"] === $tag) {
                    array_splice($this->history["messages"], $count, 1);
                }
                $count = $count + 1;
            }
        }

        $this->saveHis();   //at the end it saves the database without the message it deleted
    }

    private function loadHis() {    //function for loading the message history, works the same as functions located in DatabaseIG.php
        $fileContent = trim(file_get_contents("messages.txt", true));
		if (!$fileContent) {
			$this->history = [
				"messages" => []
			];
		} else {
			$this->history = json_decode($fileContent, true);
		}
    }

    private function saveHis() {    //function for saving the message history, works the same as functions located in DatabaseIG.php
        file_put_contents("messages.txt", json_encode($this->history), FILE_USE_INCLUDE_PATH);
    }

}

?>