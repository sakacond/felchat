//JSON script for requesting new messages from server and their printing in to the chat window without reloading the page

var lastTag = "defaultTag"; //the tag of last recieved message, if there is none such message the "defaultTag" value is sent to server
var refreshTimer = setInterval(requestMessages, 5000);  //refresher of the page, sends new requests for new messages every 5 seconds
var myDiv = document.getElementById("chatwindow");  

function requestMessages() {    //algorythm that requests messages from reqHand.php
    var tag = JSON.stringify(lastTag);  //sends the tag of the last message

    if (window.XMLHttpRequest) {
        var request = new XMLHttpRequest();
    } else {
        var request = new ActiveXObject("source/global/reqHand.php");
    }

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var messages = JSON.parse(this.responseText);   //recieved part of the database encodes
            if (messages.length <= 1) { //if there is not a single message it returns
                return;
            }
            writeMessages(messages);    //else it writes all the new messages
            myDiv.scrollTop = myDiv.scrollHeight;   //this keeps the scroll of the chat window on the bottom by default, because the new messages are being written "beforeend" 
        }
    };
    request.open("POST", "source/global/reqHand.php", true);    //default POST method for sending the requests and tag
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send("tag=" + tag);
}

function writeMessages(messages) {  //function that cycles throught the recieved messages
    var i = 0;
    var len = messages.length;
    setGlobal(messages[len - 1].tag);   //this changes the value of (the last recieved message) $tag to the most recent message tag message
    while (i < len) {
        var message = messages[i];
        writeMessage(message.tag, message.time, message.color, message.username, message.text); //cycle sends all the messages in their order to the function that writes them
        i = i + 1;
    }
}

function writeMessage(tag, time, colour, user, text) {  //function for writing the message
    var message = '<p><span class="tag">' + tag + '</span> ' + time + ' <span style="color:'+ colour +';">' + user + '</span>: ' + text + '</p>'; //this is the format of the message that is being added to the chatwindow, note that the style is used only because of the color change of the name and it is the only occassion when I used it in html code

    document.getElementById("chatwindow").insertAdjacentHTML("beforeend", message); //inserts message into the chatWindow element
}

function setGlobal(newTag) {    //function for setting the $tag value
    lastTag = newTag;
}

requestMessages();  //this requests new messages instantly when you first open up the page, so you do not have to wait 5 seconds