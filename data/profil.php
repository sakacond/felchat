<?php

require "source/app.php";

function escape($value) {
    echo htmlspecialchars($value, ENT_QUOTES);
}

$skin = isset($_COOKIE["skin"]);

$emailFormSent = isset($_POST["esub"]);

$passValid = false;
$emailAvailiable = false;
$emailValid = false;

//php validator of the email change form
if ($emailFormSent) {
    $emailValid = isset($_POST["email"]) && strpos($_POST["email"], "@");
    $emailAvailiable = isset($_POST["email"]) && !$db->emailUsed($_POST["email"]);
    $passValid = isset($_POST["pass"]) && password_verify($_POST["pass"], $user->userLogged["password"]);
    
    $emailFormValid = $passValid && $emailAvailiable && $emailValid;

    if ($emailFormValid) {
        $db->changeEmail($user->userLogged["username"], $_POST["email"]);
        header("Location: profil.php");
        exit();
    }
}

$passFormSent = isset($_POST["psub"]);

$oldPassValid = false;
$npassValid = false;
$passSame = false;

//php validator of the password change form
if ($passFormSent) {
    $npassValid = isset($_POST["newPass"]) && strlen($_POST["newPass"]) >= 8 && strlen($_POST["newPass"]) <= 30;
    $passSame = isset($_POST["newPass"]) && isset($_POST["passcheck"]) && $_POST["newPass"] === $_POST["passcheck"];
    $oldPassValid = isset($_POST["oldPass"]) && password_verify($_POST["oldPass"], $user->userLogged["password"]);
    
    $passFormValid = $oldPassValid && $npassValid && $passSame;

    if ($passFormValid) {
        $db->changePassword($user->userLogged["username"] ,$_POST["newPass"]);
        header("Location: profil.php");
        exit();
    }
}

$delFormSent = isset($_POST["delete"]);
$passwordValid = false;

//php validator of the user delete form
if($delFormSent) {
    $passwordValid = password_verify($_POST["password"], $user->userLogged["password"]);
    if($passwordValid) {
        $db->deleteUser($user->userLogged["username"]);
        header("Location: uvod.php");
        exit();
    }
}

$skinFormSent = isset($_POST["skinChange"]);
$skin = isset($_COOKIE["skin"]);

//php validator of the skin change form
if ($skinFormSent) {
    if (!$skin) {
        setcookie("skin", "1", time() + 86400);
    } else {
        setcookie("skin", null, time() + 86400);
    }
    header("Location: profil.php");
    exit();
}

$nameColorFormSent = isset($_POST["colorChange"]);

//php validator for the username color change form
if ($nameColorFormSent) {
    $color = $_POST["nameColor"];
    $db->changeColor($user->userLogged["username"], $color);
    header("Location: profil.php");
    exit();
}

?>
<!DOCTYPE html>
<html class="<?php if ($skin) echo 'day';?>" lang="cs">
    <head>
        <title>
            FELchat - <?php echo ($user->userLogged["username"]); ?> 
        </title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="source/felchat-style.css">
    </head>
    <body>
        <header>
            <a class="topbar" href="index.php"><h1>FELchat</h1></a>
            <ul class="<?php if ($user->userLogged !== null) echo 'toolbar'; else echo 'hidden';?>" id="links-left">    
                <li><a href="profil.php" class="active"><h1>
                    <?php
                    if ($user->userLogged === null) {
                        echo 'Not Empty Heading';
                    } else {
                        $name = $user->userLogged["username"]; 
                        if (strlen($name) <= 12) {
                            echo ($name);
                        } else {
                            $shrtName = substr($name, 0, 9);
                            echo ($shrtName); ?>...<?php }
                    }
                    ?></h1></a></li>
                <li class="<?php if ($db->adminExists($user->userLogged["username"])) echo 'show'; else echo 'hidden';?>"><a href="uzivatele.php?u=0&a=0"><h1>Uživatelé</h1></a></li>
                <li><a href="logOut.php" id="pseudo-links-right"><h1>Odhlášení</h1></a></li> 
            </ul> 
            <ul class="<?php if ($user->userLogged !== null) echo 'hidden'; else echo 'toolbar';?>" id="links-right">   
                <li><a href="registrace.php"><h1>Registrace</h1></a></li>
                <li><a href="prihlaseni.php"><h1>Přihlášení</h1></a></li>
            </ul>
            <ul id="roombar" class="show">
                <li><h1>Místnosti:</h1></li>
                <li><a href="global.php"><h1>/ Globální chat /</h1></a></li>
                <li><a href="skola.php"><h1>/ Škola /</h1></a></li>
                <li><a href="gaming.php"><h1>/ Gaming /</h1></a></li>
                <li><a href="hobby.php"><h1>/ Hobby /</h1></a></li>
                <li><a href="pap.php"><h1>/ Párty a posezení /</h1></a></li>
            </ul>
        </header>
        <main>           
            <div class="maininfo" id="profil">
                <h2>Jméno:</h2>
                <h3 style="color:<?php echo $user->userLogged["color"];?>;"><?php echo ($user->userLogged["username"]); ?></h3>
                <h2>Email:</h2>
                <h3><?php echo ($user->userLogged["email"]); ?></h3>
                <h2>Skin:</h2>
                <div>
                    <form action="profil.php" id="skinForm" method="POST">
                        <div>Aktuálí skin: <?php if ($skin) echo 'day'; else echo 'night';?></div>
                        <br>
                        <button type="submit" name="skinChange">Změnit</button>
                    </form>
                </div>
                <h2>Barva jména:</h2>
                <div>
                    <form action="profil.php" id="colorForm" method="POST">
                        <div><label>Barva jména v chatu: <input type="color" name="nameColor" value="<?php echo $user->userLogged["color"];?>"></label></div>
                        <br>
                        <button type="submit" name="colorChange">Změnit</button>
                    </form>
                </div>
            </div>
            <div class="side">
                <form id="emailCh" action="profil.php" method="POST">
                    <fieldset>
                        <legend>Změna e-mailu:</legend>
                        <br>
                        <div>
                            <label>Nový e-mail: <input id="email" name="email" type="email" value="<?php if (isset($_POST["email"])) escape($_POST["email"]) ?>" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"></label>
                            <?php if ($emailFormSent && !$emailValid) { ?>
                                <div class="formerror">E-mail byl nesprávně zadán.</div>
                            <?php } else if ($emailFormSent && !$emailAvailiable) { ?>
                                <div class="formerror">E-mail už byl použit. Použijte prosím jiný.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <label>Heslo: <input id="pass" name="pass" type="password" value="<?php if (isset($_POST["pass"])) escape($_POST["pass"]) ?>" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($emailFormSent && !$passValid) { ?>
                                <div class="formerror">Nesprávné heslo. Zkuste znovu.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <button name="esub" type="submit">Změnit</button>
                        </div>
                    </fieldset>
                </form>
                <form id="passCh" action="profil.php" method="POST">
                    <fieldset>
                        <legend>Změna hesla:</legend>
                        <div>
                            <label>Staré heslo: <input id="oldPass" name="oldPass" type="password" value="<?php if (isset($_POST["oldPass"])) escape($_POST["oldPass"]) ?>" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($passFormSent && !$oldPassValid) { ?>
                                <div class="formerror">Nesprávné heslo. Zkuste znovu.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <label>Nové heslo: <input id="newPass" name="newPass" type="password" value="<?php if (isset($_POST["newPass"])) escape($_POST["newPass"]) ?>" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($passFormSent && !$npassValid) { ?>
                                <div class="formerror">Heslo musí mít alespoň 8 - 30 znaků.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <label>Nové heslo znovu: <input id="passcheck" name="passcheck" type="password" value="<?php if (isset($_POST["passcheck"])) escape($_POST["passcheck"]) ?>" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($passFormSent && !$passSame) { ?>
                                <div class="formerror">Hesla se nezhodují.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <div>
                            <button name="psub" type="submit">Změnit</button>
                        </div>
                    </fieldset>
                </form>
                <form action="profil.php" method="POST" id="formDel">
                    <fieldset>
                        <legend>Vymazat účet:</legend>
                        <br>
                        <div>
                            <label>Heslo: <input id="password" name="password" type="password" value="<?php if (isset($_POST["password"])) escape($_POST["password"]) ?>" required pattern=".{8,30}" title="Heslo musí mít 8 - 30 znaků."></label>
                            <?php if ($delFormSent && !$passwordValid) { ?>
                                <div class="formerror">Nesprávné heslo. Zkuste znovu.</div>
                            <?php } ?>
                        </div>
                        <br>
                        <button name="delete" type="submit">Smazat</button>
                    </fieldset>
                </form>
            </div>
        </main>
        <footer>
            
        </footer>

        <script src="source/FELchat.js"></script>
    
    </body>
</html>